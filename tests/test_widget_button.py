import themed_tkinter as ttk

from custom_test_case import ScreenshotTestCase


class WidgetButtonTestCase(ScreenshotTestCase):

    def test_button(self):
        def create_widget(root):
            button1 = ttk.Button(root, text="Hello world")
            button1.pack()
            button2 = ttk.PrimaryButton(root, text="Bonjour tous le monde")
            button2.pack()
        self.assertScreenshot("button", create_widget)
