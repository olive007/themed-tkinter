import themed_tkinter as ttk

from custom_test_case import ScreenshotTestCase


class WidgetEntryTestCase(ScreenshotTestCase):

    def test_entry(self):
        def create_widget(root):
            entry = ttk.Entry(root)
            entry.pack()
        self.assertScreenshot("entry", create_widget)
