import themed_tkinter as ttk

from custom_test_case import ScreenshotTestCase


class WidgetLabelTestCase(ScreenshotTestCase):

    def test_label(self):
        def create_widget(root):
            label1 = ttk.Label(root, text="Hello world")
            label1.pack()
            label2 = ttk.Label(root, text="Bonjour tous le monde")
            label2.pack()
        self.assertScreenshot("label", create_widget)
