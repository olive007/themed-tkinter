import unittest
import themed_tkinter as ttk
from themed_tkinter.color import *


class UtilTestCase(unittest.TestCase):

    def test_color(self):
        self.assertEqual("#E81123", get_color('red'))
        self.assertEqual("#E81123", get_color('RED'))
        self.assertEqual("#06B025", get_color('green'))
        self.assertEqual("#DDD", get_color('main_bg'))
        self.assertEqual("#222", get_color('main_text'))

        tk = ttk.Tk()
        tk.theme = 'dark'
        self.assertEqual("#222", get_color('main_bg'))
        self.assertEqual("#DDD", get_color('main_text'))

        tk.theme = 'light'
        self.assertEqual("#DDD", get_color('main_bg'))
        self.assertEqual("#222", get_color('main_text'))


