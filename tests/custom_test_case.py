import os
import unittest
import themed_tkinter as ttk
import pyautogui


from os import path
from PIL import Image, ImageChops

from themed_tkinter.util import get_widget_region, get_window_region


class ScreenshotTestCase(unittest.TestCase):

    screenshot_dir = path.join(path.dirname(__file__), "screenshot")
    screenshot_diff_dir = path.join(path.dirname(__file__), "screenshot_diff")

    @classmethod
    def setUpClass(cls):
        if not path.isdir(cls.screenshot_dir):
            os.makedirs(cls.screenshot_dir)
        if not path.isdir(cls.screenshot_diff_dir):
            os.makedirs(cls.screenshot_diff_dir)

    @classmethod
    def tearDownClass(cls):
        files = os.listdir(cls.screenshot_diff_dir)
        if len(files) == 0:
            os.rmdir(cls.screenshot_diff_dir)

    def assertScreenshot(self, file_name, function, geometry="600x400+100+100"):
        def run_for_theme(theme):
            def after_start():
                screenshot_now = pyautogui.screenshot(region=get_window_region(root)).convert('RGB')
                if not path.isfile(file_path):
                    screenshot_now.save(file_path)
                else:
                    screenshot_saved = Image.open(file_path).convert('RGB')
                    diff = ImageChops.difference(screenshot_now, screenshot_saved)
                    if diff.getbbox():
                        screenshot_now.save(new_file_path)
                        diff.save(diff_file_path)
                # We use 'root.quit()' instead of 'root.destroy()' because the library start a random window
                root.after(1000, lambda: root.quit())

            file_path = path.join(ScreenshotTestCase.screenshot_dir, f"{file_name}_{theme}.png")
            new_file_path = path.join(ScreenshotTestCase.screenshot_diff_dir, f"{file_name}_{theme}.png")
            diff_file_path = path.join(ScreenshotTestCase.screenshot_diff_dir, f"{file_name}_{theme}_diff.png")
            if path.isfile(new_file_path):
                os.remove(new_file_path)
            if path.isfile(diff_file_path):
                os.remove(diff_file_path)

            root = ttk.Tk()
            root.geometry(geometry)
            root.theme = theme

            function(root)

            root.after(1000, after_start)
            root.mainloop()
            self.assertFalse(path.isfile(diff_file_path))

        run_for_theme("light")
        run_for_theme("dark")
