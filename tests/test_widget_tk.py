import themed_tkinter as ttk

from themed_tkinter.widget.tk import Theme
from custom_test_case import ScreenshotTestCase


class WidgetTkTestCase(ScreenshotTestCase):

    def test_attr(self):
        root = ttk.Tk()
        self.assertEqual(0, len(root._ThemedWidget__updated_kwargs))

    def test_theme(self):
        def set_theme(value):
            root.theme = value
        root = ttk.Tk()

        root.theme = "auto"
        root.theme = "AUTO"
        root.theme = Theme.AUTO
        self.assertEqual("auto", root._Tk__theme_forced)
        root.theme = "light"
        root.theme = "LIGHT"
        root.theme = Theme.LIGHT
        self.assertEqual("light", root.theme)
        root.theme = "dark"
        root.theme = "DARK"
        root.theme = Theme.DARK
        self.assertEqual("dark", root.theme)
        self.assertRaises(AttributeError, lambda: set_theme(0))
        self.assertRaises(AttributeError, lambda: set_theme(1))
        self.assertRaises(AttributeError, lambda: set_theme(None))
        self.assertRaises(RuntimeError, lambda: set_theme("pink"))
        self.assertRaises(RuntimeError, lambda: set_theme("PINK"))
