import unittest
import tkinter as tk

from themed_tkinter.util import *


class UtilTestCase(unittest.TestCase):

    def test_color_is_light(self):
        # White
        self.assertTrue(color_is_light("#DDD")),
        self.assertTrue(color_is_light("#FFFFFFFFF"))
        self.assertTrue(color_is_light("#FFF"))
        self.assertTrue(color_is_light("#FFFFFF"))
        # Yellow
        self.assertTrue(color_is_light("#FFE78F"))
        # Blue
        self.assertTrue(color_is_light("#429CE3"))
        # Cyan
        self.assertTrue(color_is_light("#BAE1E6"))
        self.assertTrue(color_is_light("#6FDAE6"))
        # Green
        self.assertTrue(color_is_light("#06B025"))

        # Black
        self.assertFalse(color_is_light("#000000000"))
        self.assertFalse(color_is_light("#000000"))
        self.assertFalse(color_is_light("#000"))
        self.assertFalse(color_is_light("#111"))
        self.assertFalse(color_is_light("#222"))
        # Red
        self.assertFalse(color_is_light("#E81123"))
        # Blue
        self.assertFalse(color_is_light("#6666EE"))
        # Magenta
        self.assertFalse(color_is_light("#E62EC0"))

    def test_get_widget_region(self):
        root = tk.Tk()
        root.geometry("400x300+200+100")
        root.overrideredirect(True)

        frame = tk.Frame(root, bg="#000")
        frame.pack(padx=10, pady=10, ipadx=10, ipady=10, fill=tk.BOTH, expand=True)

        region = get_widget_region(root)
        self.assertEqual(200, region.x)
        self.assertEqual(100, region.y)
        self.assertEqual(400, region.width)
        self.assertEqual(300, region.height)
        region = get_widget_region(frame)
        self.assertEqual(210, region.x)
        self.assertEqual(110, region.y)
        self.assertEqual(380, region.width)
        self.assertEqual(280, region.height)

    def test_get_window_region(self):
        root = tk.Tk()
        root.geometry("400x300+200+100")
        root.overrideredirect(True)

        button = tk.Button(root, text="Press me")
        button.pack(padx=10, pady=10)

        region = get_window_region(root)
        self.assertEqual(200, region.x)
        self.assertEqual(100, region.y)
        self.assertEqual(400, region.width)
        self.assertEqual(300, region.height)

        region = get_window_region(button)
        self.assertEqual(200, region.x)
        self.assertEqual(100, region.y)
        self.assertEqual(400, region.width)
        self.assertEqual(300, region.height)
