import collections

ScreenRegion = collections.namedtuple('ScreenRegion', 'x, y, width, height')
