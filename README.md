# Themed Tkinter
Simple module help developer to generate tkinter with color matching the OS theme.


## Issues/Bug report or improvement ideas
https://gitlab.com/olive007/themed-tkinter/-/issues


## License
GNU Lesser General Public License v3 or later (LGPLv3+)
