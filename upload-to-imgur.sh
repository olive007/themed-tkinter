#!/usr/bin/env bash

[ -f upload-to-imgur-variable.sh ] && source upload-to-imgur-variable.sh

# Function to upload a path
# First argument should be a content spec understood by curl's -F option
function upload {
	curl -s -H "Authorization: Client-ID $IMGUR_CLIENT_ID" -H "Expect: " -F "image=$1" https://api.imgur.com/3/image.xml
	# The "Expect: " header is to get around a problem when using this through
	# the Squid proxy. Not sure if it's a Squid bug or what.
}

# Loop through arguments
while [ $# -gt 0 ]; do
	file="$1"
	shift

	# Upload the image
	echo "Sending file: '$file'"
	response=$(upload "@$file") 2>/dev/null
	return_code=$?

	if [ $return_code -ne 0 ]; then
		echo "Upload failed: return_code is $return_code" >&2
		continue
	elif echo "$response" | grep -q 'success="0"'; then
		echo "Error message from imgur:" >&2
		msg="${response##*<error>}"
		echo "${msg%%</error>*}" >&2
		continue
	fi

	# Parse the response and output our stuff
	url="${response##*<link>}"
	url="${url%%</link>*}"
	echo "See the screenshot here: '$url'" | sed 's/^http:/https:/'
done
